# Running this Demo
Ensure that Rails installed, and run the following commands in your shell:

```
rake db:migrate
rake db:seed
rails s
```

Your demo server will be available at http://localhost:3000/ by default